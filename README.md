# Análisis para la calibración para un sensor de presión diferencial

En el repositorio se encontrará un informe técnico para calibrar un sensor de presión usado para medir la columna de agua.
Para ello se hallaron expresiones analíticas que describen la variación de una columna de agua en un tubo que se descarga
mediante un orificio de salida.

Las expresiones analíticas fueron halladas a partir del desarrollo de Balance de materia, Balances de Cantidad de Movimiento y Balance Simplificado de Energía (Bernoulli).

- En ``Trabajo_Final_Mecanica_de_Fluidos.pdf`` se encuentra en análisis en detalle de los pasos realizados para hallar las expresiones analíticas
- En ``Columna de agua en el tiempo.ipynb`` se encuentra un script de python para visualizar la comparación de las dos metodologías utilizadas, este script fue realizado utilizando jupyter notebook.

